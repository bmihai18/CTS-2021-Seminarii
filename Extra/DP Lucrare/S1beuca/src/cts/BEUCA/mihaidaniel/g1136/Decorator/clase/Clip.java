package cts.BEUCA.mihaidaniel.g1136.Decorator.clase;



    public class Clip implements IClip {
        private String nume;
        private int durata;

        public Clip(String nume, int durata) {
            super();
            this.nume = nume;
            this.durata=durata;
        }

        public String getNume() {
            return nume;
        }

        public int getDurata() {
            return durata;
        }

        @Override
        public void pause() {
            System.out.println("Clipul "+this.getNume()+" a fost pus pe pauza");

        }

        @Override
        public void stop() {
            System.out.println("Clipul "+this.getNume()+" a fost oprit");

        }

        @Override
        public void resume() {
            System.out.println("Clipul "+this.getNume()+" a fost repornit");

        }

        @Override
        public void start() {
            System.out.println("Clipul "+this.getNume()+" a fost pornit");

        }
    }

