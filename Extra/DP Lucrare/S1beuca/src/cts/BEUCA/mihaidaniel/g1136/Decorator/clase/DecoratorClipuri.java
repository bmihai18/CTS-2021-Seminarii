package cts.BEUCA.mihaidaniel.g1136.Decorator.clase;

import cts.BEUCA.mihaidaniel.g1136.Decorator.clase.IClip;

public class DecoratorClipuri implements IClip {
    protected IClip clip;
    float durataReclama;

    public DecoratorClipuri(IClip clip, float durataReclama) {
    this.clip=clip;
    this.durataReclama=durataReclama;
    }

    public float getDurataReclama() {
        return durataReclama;
    }

    @Override
    public void pause() {
        System.out.println("Clipul cu reclama de "+ this.getDurataReclama()+" minute a fost pus pe pauza");

    }
    @Override
    public void stop() {
        System.out.println("Clipul cu reclama de "+ this.getDurataReclama()+" minute a fost oprit");

    }

    @Override
    public void resume() {
        System.out.println("Clipul cu reclama de "+ this.getDurataReclama()+" minute a fost reluat");

    }

    @Override
    public void start() {
        System.out.println("Clipul cu reclama de "+ this.getDurataReclama()+" minute a fost pornit");

    }
}
