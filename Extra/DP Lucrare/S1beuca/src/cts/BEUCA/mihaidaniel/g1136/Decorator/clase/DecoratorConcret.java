package cts.BEUCA.mihaidaniel.g1136.Decorator.clase;

public class DecoratorConcret extends DecoratorClipuri {
    private String numelePromotiei;

    public DecoratorConcret(IClip clip, float durataReclama, String numelePromotiei) {
        super(clip, durataReclama);
        this.numelePromotiei = numelePromotiei;
    }


    public void reclamaPromotiei(String numelePromotiei) {
        System.out.println("Reclama la promotia"+this.numelePromotiei);
        super.start();


    }
}
