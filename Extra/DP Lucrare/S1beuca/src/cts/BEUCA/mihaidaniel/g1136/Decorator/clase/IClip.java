package cts.BEUCA.mihaidaniel.g1136.Decorator.clase;

public interface IClip {
    void pause();

    void stop();

    void resume();

    void start();
}