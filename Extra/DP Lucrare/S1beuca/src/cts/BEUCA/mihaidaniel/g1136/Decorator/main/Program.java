package cts.BEUCA.mihaidaniel.g1136.Decorator.main;

import cts.BEUCA.mihaidaniel.g1136.Decorator.clase.Clip;
import cts.BEUCA.mihaidaniel.g1136.Decorator.clase.DecoratorClipuri;
import cts.BEUCA.mihaidaniel.g1136.Decorator.clase.DecoratorConcret;
import cts.BEUCA.mihaidaniel.g1136.Decorator.clase.IClip;

public class Program {
    public static void main(String[] args) {
        IClip clip1 = new Clip("Tutorial", 15);
        DecoratorClipuri deco = new DecoratorClipuri(clip1, 3);
        deco.start();
        deco.pause();
        deco.resume();
        deco.stop();
       DecoratorConcret decorator = new DecoratorConcret(clip1,15,"Lays");
       decorator.start();
       decorator.stop();
       decorator.resume();
       decorator.stop();

    }
}
