package cts.BEUCA.mihaidaniel.g1136.FactoryMethod.clase;



    public abstract class Clip {
        private String nume;
        private int durata;

        public Clip(String nume, int durata) {
            super();
            this.nume = nume;
            this.durata=durata;
        }

        public String getNume() {
            return nume;
        }


    }

