package cts.BEUCA.mihaidaniel.g1136.FactoryMethod.clase;

public interface IClip {
    void pause();

    void stop();

    void resume();

    void start();
}