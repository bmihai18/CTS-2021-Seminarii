package cts.BEUCA.mihaidaniel.g1136.FactoryMethod.clase;

public class Live extends Clip {

	public Live(String nume, int timp) {
		super(nume, timp);
	}

	@Override
	public String toString() {
		return "Animatie [getNume()=" + getNume() + "]";
	}



}
