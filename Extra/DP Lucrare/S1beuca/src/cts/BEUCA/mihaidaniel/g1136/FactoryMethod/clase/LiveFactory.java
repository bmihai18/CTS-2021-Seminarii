package cts.BEUCA.mihaidaniel.g1136.FactoryMethod.clase;

//Fiecare Factory implementeaza Interfata IClip

public class LiveFactory implements IClip {


    @Override
    public void pause() {
        System.out.println("S-a pus pauza live");
    }

    @Override
    public void stop() {
        System.out.println("S-a pus stop live");
    }

    @Override
    public void resume() {
        System.out.println("S-a reluat live");
    }

    @Override
    public void start() {
        System.out.println("S-a redat live");

    }
}
