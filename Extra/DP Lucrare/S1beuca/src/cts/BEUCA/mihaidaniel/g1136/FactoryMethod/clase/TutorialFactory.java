package cts.BEUCA.mihaidaniel.g1136.FactoryMethod.clase;
public class TutorialFactory implements IClip {

    @Override
    public void pause() {
        System.out.println("S-a pus pauza tutorial");

    }

    @Override
    public void stop() {
        System.out.println("S-a oprit tutorialul");

    }

    @Override
    public void resume() {
        System.out.println("S-a reluat tutorialul");

    }

    @Override
    public void start() {
        System.out.println("S-a redat tutorialul");

    }
}