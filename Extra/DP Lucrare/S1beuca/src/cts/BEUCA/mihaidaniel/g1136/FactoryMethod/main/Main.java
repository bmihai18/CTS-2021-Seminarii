package cts.BEUCA.mihaidaniel.g1136.FactoryMethod.main;


import cts.BEUCA.mihaidaniel.g1136.FactoryMethod.clase.*;

public class Main {
    public static void printClip(IClip factory, String nume)
    {
        factory.start();
        factory.pause();
        factory.resume();
        factory.stop();



    }
    public static void main(String[] args) {
        printClip(new LiveFactory(), "CTS");
        printClip(new TutorialFactory(),"OOP");
    }
}
