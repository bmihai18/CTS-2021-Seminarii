package src.cts.beuca.mihaidaniel.g1136.main;


import src.cts.beuca.mihaidaniel.g1136.pattern.command.*;

public class MainCommand {

	public static void main(String[] args) {
		IProcesatorCerere modul =new ModulRobot("robot1");
		
		OperatorCereri secretariat = new OperatorCereri();
		
		Command cerere1 = new CerereAdeverinta(modul, TipCerere.NORMALA, "denumire 1");
		Command cerere2 = new CerereAdeverinta(modul, TipCerere.NORMALA, "denumire 1");
		Command cerere3 = new CerereAdeverinta(modul, TipCerere.URGENTA, "denumire 2");
		Command cerere4 = new CerereAdeverinta(modul, TipCerere.URGENTA, "denumire 2");
		Command cerere5 = new CerereAdeverinta(modul, TipCerere.URGENTA, "denumire 2");
		
		secretariat.inregistrareCerere(cerere1);
		secretariat.inregistrareCerere(cerere2);
		secretariat.inregistrareCerere(cerere3);
		secretariat.inregistrareCerere(cerere4);
		secretariat.inregistrareCerere(cerere5);
		
		secretariat.procesareCerere();
		secretariat.procesareCerere();
		secretariat.procesareCerere();
		secretariat.procesareCerere();
		secretariat.procesareCerere();
		secretariat.procesareCerere();
	}

}
