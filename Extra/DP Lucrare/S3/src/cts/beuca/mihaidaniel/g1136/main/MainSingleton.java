package src.cts.beuca.mihaidaniel.g1136.main;

import java.util.HashMap;

import src.cts.beuca.mihaidaniel.g1136.pattern.singleton.IRobotSoftware;
import src.cts.beuca.mihaidaniel.g1136.pattern.singleton.RobotSoftware;


public class MainSingleton {
	
	public static void main(String[] args) {
		HashMap<String, String> informatii = new HashMap<String, String>();
		informatii.put("alerte", "exista o alerta");
		informatii.put("secretariat", "este vacanta");
		
		HashMap<String, String> informatii2 = new HashMap<String, String>();
		informatii2.put("alerte", "NU exista alerte");
		informatii2.put("secretariat", "eliberam azi adeverinte");
		
		IRobotSoftware robot1 = RobotSoftware.getInstanta(12, informatii);
		IRobotSoftware robot2 = RobotSoftware.getInstanta(14, informatii2);
		
		System.out.println(robot1);
		System.out.println(robot2);
		
		System.out.println(robot1.getInformatii("alerte"));
		System.out.println(robot2.getInformatii("alerte"));
		
		System.out.println(robot1.getIdRobot());
		System.out.println(robot2.getIdRobot());
		
		robot1.prelucrareCerere("alerta");
		robot2.prelucrareCerere("alerta");
		
	}

}
