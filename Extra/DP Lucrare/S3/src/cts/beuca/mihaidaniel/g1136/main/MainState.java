package src.cts.beuca.mihaidaniel.g1136.main;

import src.cts.beuca.mihaidaniel.g1136.pattern.state.CerereStudent;

public class MainState {
	public static void main(String[] args) {
		CerereStudent cerere1 = new CerereStudent(2, true, false);
		CerereStudent cerere2 = new CerereStudent(11, true, true);
		CerereStudent cerere3 = new CerereStudent(2, true, true);
		
		cerere1.verificare();
		cerere2.verificare();
		cerere3.verificare();
		
		System.out.println("Confirmare:");
		cerere1.confirmare();
		cerere2.confirmare();
		cerere3.confirmare();
		
		System.out.println("Avizare:");
		cerere1.avizareDecanat();
		cerere2.avizareDecanat();
		cerere3.avizareDecanat();
	}

}
