package src.cts.beuca.mihaidaniel.g1136.pattern.command;

public class CerereAdeverinta implements Command {
	
	private IProcesatorCerere modul;	
	private TipCerere tip;
	private String denumire;
	
	public CerereAdeverinta(IProcesatorCerere modul, TipCerere tip, String denumire) {
		super();
		this.modul = modul;
		this.tip = tip;
		this.denumire = denumire;
	}
	
	

	public TipCerere getTip() {
		return tip;
	}



	public void setTip(TipCerere tip) {
		this.tip = tip;
	}



	@Override
	public void proceseazaComanda() {
		this.modul.procesareCerere(this.tip, this.denumire);
	}

}
