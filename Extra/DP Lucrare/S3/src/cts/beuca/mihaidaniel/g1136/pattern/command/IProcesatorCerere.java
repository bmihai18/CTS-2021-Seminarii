package src.cts.beuca.mihaidaniel.g1136.pattern.command;

public interface IProcesatorCerere {
    public void procesareCerere(TipCerere tip, String denumire);
}
