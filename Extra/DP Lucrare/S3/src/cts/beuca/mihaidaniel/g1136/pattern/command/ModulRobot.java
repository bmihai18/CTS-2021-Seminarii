package src.cts.beuca.mihaidaniel.g1136.pattern.command;

public class ModulRobot implements IProcesatorCerere {
	
	private String numeModul;
	
	public ModulRobot(String numeModul) {
		super();
		this.numeModul = numeModul;
	}

	@Override
	public void procesareCerere(TipCerere tip, String denumire) {
		System.out.println("Modulul " + this.numeModul + " a procesat cerere "
	+ tip + " cu denumirea " + denumire);

	}

}
