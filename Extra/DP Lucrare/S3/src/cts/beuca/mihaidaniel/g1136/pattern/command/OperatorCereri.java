package src.cts.beuca.mihaidaniel.g1136.pattern.command;

import java.util.ArrayList;
import java.util.List;

public class OperatorCereri {
	
	private List<Command> cereri;

	public OperatorCereri() {
		super();
		this.cereri = new ArrayList<Command>();
	}
	
	public void inregistrareCerere(Command command) {	
		this.cereri.add(command);
	}
	
	public void procesareCerere() {
		if(this.cereri.isEmpty()) {
			System.out.println("Nu exista cereri");
			return;
		}
		Command comandaDeExecutat = null;
		for(Command command : cereri) {
			if(command instanceof CerereAdeverinta) {
				CerereAdeverinta cerereAdeverinta = (CerereAdeverinta) command;
				if(cerereAdeverinta.getTip() == TipCerere.URGENTA) {
					comandaDeExecutat = command;
					command.proceseazaComanda();
					break;
				}
			} else {
				CererePlataTaxa cererePlataTaxa = (CererePlataTaxa) command;
				if(cererePlataTaxa.getTip() == TipCerere.URGENTA) {
					comandaDeExecutat = command;
					command.proceseazaComanda();
					break;
				}
			}
		}
		if(comandaDeExecutat == null) {
			comandaDeExecutat = this.cereri.get(0);
			comandaDeExecutat.proceseazaComanda();
		}
		this.cereri.remove(comandaDeExecutat);
	}

}
