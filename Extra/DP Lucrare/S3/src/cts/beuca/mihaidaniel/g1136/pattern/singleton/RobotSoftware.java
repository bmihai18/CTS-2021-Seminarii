package src.cts.beuca.mihaidaniel.g1136.pattern.singleton;

import java.util.HashMap;

public class RobotSoftware implements IRobotSoftware {
	
	private int idRobot;
	private HashMap<String, String> informatii;
	
	private static RobotSoftware instanta = null;

	private RobotSoftware(int idRobot, HashMap<String, String> informatii) {
		super();
		this.idRobot = idRobot;
		this.informatii = informatii;
	}
	
	public static synchronized RobotSoftware getInstanta(int idRobot, HashMap<String, String> informatii) {
		if(instanta==null) {
			instanta = new RobotSoftware(idRobot, informatii);
		}
		return instanta;
	}

	@Override
	public void trimiteCerere(String denumire) {
		System.out.println("Robotul " + this.idRobot + " a trimis cererea " +  denumire);
	}

	@Override
	public String getInformatii(String categorie) {
		return this.informatii.get(categorie);
	}

	@Override
	public void prelucrareCerere(String tip) {
		System.out.println("Robotul " + this.idRobot + " a prelucrat cererea " +  tip);
	}

	@Override
	public int getIdRobot() {
		return this.idRobot;
	}

	@Override
	public String toString() {
		return "RobotSoftware [idRobot=" + idRobot + ", informatii=" + informatii + "]";
	}
	
	

}
