package src.cts.beuca.mihaidaniel.g1136.pattern.state;

public class CerereStudent implements ICerereStudent {
	
	private int numarZile;
	private StareCerere stare;
	private boolean esteCompleta;
	private boolean areMotiv;
	
	public CerereStudent(int numarZile, boolean esteCompleta, boolean areMotiv) {
		super();
		this.numarZile = numarZile;
		this.esteCompleta = esteCompleta;
		this.areMotiv = areMotiv;
		this.stare = new StareCerereTrimisaPeFlux("Cerere trimisa pe flux");
	}

	@Override
	public void confirmare() {
		if(this.numarZile <= 10) {
			this.stare = new StareCerereConfirmata("Cererea este confirmata");
			this.stare.gestioneazaCerere();
		}
	}

	@Override
	public void verificare() {
		if(this.numarZile > 10) {
			respingere();
			return;
		}
		if(this.stare instanceof StareCerereConfirmata) {
			System.out.println("Cerere primita");
		} 
		
		if(this.esteCompleta) {
			avizareDecanat();
			return;
		} else {
			respingere();
			return;
		}
	}

	@Override
	public void avizareDecanat() {
		if(this.areMotiv) {
			this.stare = new StareCerereAvizata("Cererea este avizata");
			this.stare.gestioneazaCerere();
		} else {
			respingere();
		}
	}

	@Override
	public void respingere() {
		this.stare = new StareCerereRespinsa("Cerere respinsa");
		this.stare.gestioneazaCerere();
	}

}
