package src.cts.beuca.mihaidaniel.g1136.pattern.state;

public interface ICerereStudent {
    public void confirmare();

    public void verificare();

    public void avizareDecanat();

    public void respingere();
}
