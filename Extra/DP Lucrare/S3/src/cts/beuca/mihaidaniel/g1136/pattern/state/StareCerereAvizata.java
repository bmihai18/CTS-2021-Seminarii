package src.cts.beuca.mihaidaniel.g1136.pattern.state;

public class StareCerereAvizata implements StareCerere {
	
	private String mesaj;
	
	public StareCerereAvizata(String mesaj) {
		super();
		this.mesaj = mesaj;
	}
	
	public void setMesaj(String mesaj) {
		this.mesaj = mesaj;
	}

	@Override
	public void gestioneazaCerere() {
		System.out.println(mesaj);
	}

}
