package src.cts.beuca.mihaidaniel.g1136.pattern.state;

public class StareCerereTrimisaPeFlux implements StareCerere {
	
	private String mesaj;

	public void setMesaj(String mesaj) {
		this.mesaj = mesaj;
	}

	public StareCerereTrimisaPeFlux(String mesaj) {
		super();
		this.mesaj = mesaj;
	}

	@Override
	public void gestioneazaCerere() {
		System.out.println(mesaj);
	}

}
