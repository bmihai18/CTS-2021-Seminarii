package cts.beuca.mihaidaniel.g1136.pattern.ChainOfResponsability.clase;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class CazGrav extends Fisa {
    private Fisa succesor;
    TipAccident tip;
    boolean medic;


    public CazGrav(String nume, TipAccident tip, boolean medic) {
        super(tip,medic);
        this.numePersoana = nume;
        this.medic = TRUE;
        this.esteConstient = FALSE;
        this.areMembreRupte = FALSE;
        this.areRaniDeschise = TRUE;
        this.sePoateDeplasa = FALSE;

    }

    @Override
    public void iaDecizie(TipAccident tip) {
        if (tip == TipAccident.grav) {

            System.out.println("Pacientul" + this.numePersoana + "cu varsta" + varsta + "eConstient:" + esteConstient + "areMembreRupte:" + areMembreRupte + "sePoateDeplasa:" + sePoateDeplasa + "areRaniDeschise:" + areRaniDeschise);
            if (medic = TRUE) {
                System.out.println("Chemam medic. Sunam salvare.");

            } else {
                System.out.println("No medic, call 112.");

                System.out.println("Chemam salvare! E grav!");
            }
        }
    }
}