package cts.beuca.mihaidaniel.g1136.pattern.ChainOfResponsability.clase;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class CazMediu extends Fisa {
    private Fisa succesor;
    TipAccident tip;
    boolean medic;


    public CazMediu(String nume, TipAccident tip, boolean medic) {
        super(tip,medic);
        this.numePersoana = nume;
        this.medic = medic;
        this.esteConstient = TRUE;
        this.areMembreRupte = TRUE;
        this.areRaniDeschise = TRUE;
        this.sePoateDeplasa = TRUE;


    }


    @Override
    public void iaDecizie(TipAccident tip) {
        if (tip == TipAccident.mediu) {

            System.out.println("Pacientul" + this.numePersoana + "cu varsta" + varsta + "eConstient:" + esteConstient + "areMembreRupte:" + areMembreRupte + "sePoateDeplasa:" + sePoateDeplasa + "areRaniDeschise:" + areRaniDeschise);
            if (medic = TRUE) {
                System.out.println("Chemam medic. Tratam persoana.");

            }
            else {
                System.out.println("No medic, call 112.");
            }
            System.out.println("trateaza accidentul mediu");

        }
    }
}