package cts.beuca.mihaidaniel.g1136.pattern.ChainOfResponsability.clase;

//MODUL ABSTRACT
public abstract class Fisa extends FisaAccident {
	private Fisa succesor;
	TipAccident tip;
	boolean medic;


	
	public Fisa(TipAccident tip, boolean medic) {
		super();
		this.succesor = null;
		this.tip = tip;
		this.medic=medic;
	}




	public TipAccident getTip() {
		return tip;
	}

	public Fisa getSuccesor() {
		return succesor;
	}

	public void setSuccesor(Fisa succesor) {
		this.succesor = succesor;
	}


	public abstract void iaDecizie(TipAccident tip);
}
