package cts.beuca.mihaidaniel.g1136.pattern.ChainOfResponsability.main;

import cts.beuca.mihaidaniel.g1136.pattern.ChainOfResponsability.clase.*;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class Program {
    public static void main(String[] args) {
        Fisa fisa1 = new CazUsor("Arnold", TipAccident.usor, TRUE);
        fisa1.iaDecizie(TipAccident.usor);
        Fisa fisa2 = new CazMediu("Marin", TipAccident.mediu,TRUE);
        fisa2.iaDecizie(TipAccident.mediu);
        Fisa fisa3 = new CazGrav("Amanda", TipAccident.grav,FALSE);
        fisa3.iaDecizie(TipAccident.grav);
        Fisa fisa4 = new CazUsor("Andrei", TipAccident.usor, FALSE);
        fisa4.iaDecizie(TipAccident.usor);

    }
}


