package cts.beuca.mihaidaniel.g1136.pattern.Strategy.clase;

public class ExercitiuFandari implements IChallenge {
    private String numeAbonat;

    public ExercitiuFandari(String numeAbonat) {
        this.numeAbonat = numeAbonat;
    }

    @Override
    public void startExercitiu(int nrRepetitii) {
        System.out.println(numeAbonat + " va face " + nrRepetitii + " repetitii din exercitiul fandare.");
    }
}
