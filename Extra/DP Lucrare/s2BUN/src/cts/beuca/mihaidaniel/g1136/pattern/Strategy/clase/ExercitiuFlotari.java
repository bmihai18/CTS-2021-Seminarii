package cts.beuca.mihaidaniel.g1136.pattern.Strategy.clase;

public class ExercitiuFlotari implements IChallenge{
    private String numeAbonat;

    public ExercitiuFlotari(String numeAbonat) {
        this.numeAbonat = numeAbonat;
    }

    @Override
    public void startExercitiu(int nrRepetitii) {
        System.out.println(numeAbonat + " va face " + nrRepetitii + " repetitii din exercitiul flotari.");
    }
}
