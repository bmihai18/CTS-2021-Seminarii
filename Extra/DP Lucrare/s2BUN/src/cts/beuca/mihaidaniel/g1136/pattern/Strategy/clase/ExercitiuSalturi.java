package cts.beuca.mihaidaniel.g1136.pattern.Strategy.clase;

public class ExercitiuSalturi implements IChallenge{
    private String numeAbonat;

    public ExercitiuSalturi(String numeAbonat) {
        this.numeAbonat = numeAbonat;
    }

    @Override
    public void startExercitiu(int nrRepetitii) {
        System.out.println(numeAbonat + " va face " + nrRepetitii + " repetitii din exercitiul salturi.");
    }
}
