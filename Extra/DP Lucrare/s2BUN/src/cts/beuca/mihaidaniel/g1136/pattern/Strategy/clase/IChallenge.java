package cts.beuca.mihaidaniel.g1136.pattern.Strategy.clase;

public interface IChallenge {
    public void startExercitiu(int nrRepetitii);
}
