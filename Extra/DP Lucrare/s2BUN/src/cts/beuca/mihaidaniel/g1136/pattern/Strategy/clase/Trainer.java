package cts.beuca.mihaidaniel.g1136.pattern.Strategy.clase;

public class Trainer {
    private String numeTrainer;
    private IChallenge challenge;

    public Trainer(String numeTrainer) {
        this.numeTrainer = numeTrainer;
        this.challenge = new ExercitiuFlotari("");
    }

    public Trainer(String numeTrainer, IChallenge challenge) {
        this.numeTrainer = numeTrainer;
        this.challenge = challenge;
    }

    public void setChallenge(IChallenge challenge) {
        this.challenge = challenge;
    }

    public void lansareChallange(int nrRepetii){
        challenge.startExercitiu(nrRepetii);
    }
}
