package cts.beuca.mihaidaniel.g1136.pattern.Strategy.main;

import cts.beuca.mihaidaniel.g1136.pattern.Strategy.clase.ExercitiuFandari;
import cts.beuca.mihaidaniel.g1136.pattern.Strategy.clase.ExercitiuFlotari;
import cts.beuca.mihaidaniel.g1136.pattern.Strategy.clase.ExercitiuSalturi;
import cts.beuca.mihaidaniel.g1136.pattern.Strategy.clase.Trainer;

public class TestStrategy {
    public static void main(String[] args) {
        Trainer trainer1 = new Trainer("Trainer Ion");
        Trainer trainer2 = new Trainer("Trainer Adela");

        trainer1.setChallenge(new ExercitiuFlotari("Abonat1"));
        trainer1.lansareChallange(3);

        trainer1.setChallenge(new ExercitiuSalturi("Abonat1"));
        trainer1.lansareChallange(5);

        trainer1.setChallenge(new ExercitiuFandari("Abonat2"));
        trainer1.lansareChallange(6);
        trainer2.setChallenge(new ExercitiuSalturi("Abonat3"));
        trainer2.lansareChallange(11);

    }
}
