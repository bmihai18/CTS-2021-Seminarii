package clase;

public abstract class BarieraAbstracta implements clase.IBariera {
    private final long durata;
    private final TipBariera tipBariera;
    private String nume;

    public BarieraAbstracta(long durata, TipBariera tipBariera, String nume) {
        super();
        this.durata = durata;
        this.tipBariera = tipBariera;
        this.nume = nume;
    }


    public final void daAcces(){
         pornesteAvertizareSonora(durata);
         pornesteLumini(durata);
         deschideBariera();
     }
        }


