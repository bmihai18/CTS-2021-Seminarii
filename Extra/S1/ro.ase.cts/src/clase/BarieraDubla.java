package clase;

public class BarieraDubla extends BarieraAbstracta {

   private long durata;

    public BarieraDubla(long durata, TipBariera tipBariera, String nume) {
        super(durata, tipBariera, nume);

    }





    @Override
    public void pornesteAvertizareSonora(long durata) {
        System.out.println("S-a pornit sunetul pentru bariera timp de "+ durata+"minute");

    }

    @Override
    public void pornesteLumini(long durata) {
        System.out.println("S-au pornit luminile pentru bariera timp de" +durata +"minute" ) ;

    }

    @Override
    public void deschideBariera() {
        System.out.println("S-a deschis bariera");

    }
}
