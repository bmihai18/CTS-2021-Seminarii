package clase;

public class BarieraOrizontala extends BarieraAbstracta {

    private long durata;


    public BarieraOrizontala(long durata, TipBariera tipBariera, String nume) {

        super(durata, tipBariera, nume);
    }


    @Override
    public void pornesteAvertizareSonora(long durata) {
        System.out.println("S-a pornit sunet pentru bariera" +durata+"minute");

    }

    @Override
    public void pornesteLumini(long durata) {
        System.out.println("S-au pornit lumini pentru bariera" +durata+"minute");

    }

    @Override
    public void deschideBariera() {
        System.out.println("S-a deschis bariera");

    }
}
