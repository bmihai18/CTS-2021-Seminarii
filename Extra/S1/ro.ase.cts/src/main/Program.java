package main;

import clase.*;

public class Program {
    public static void main(String[] args) {
        int a=0; //exemplu pentru code coverage
        if(a==1) {
            BarieraAbstracta barieraOrizontala = new clase.BarieraOrizontala(3, TipBariera.Orizontala, "Bariera Orizontaka");
            barieraOrizontala.daAcces();
        }else {
            BarieraAbstracta barieraVerticala = new BarieraVerticala(5, TipBariera.Verticala, "Bariera verticala");
            barieraVerticala.daAcces();

            BarieraAbstracta barieraDubla = new clase.BarieraDubla(7, TipBariera.Dubla, "Dubla");
            barieraDubla.daAcces();
        }

    }
}
