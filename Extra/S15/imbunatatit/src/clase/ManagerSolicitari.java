package clase;

import java.util.ArrayList;
import java.util.List;
public class ManagerSolicitari {
    private List<Command> solicitari;

    public ManagerSolicitari() {
        super();
        this.solicitari = new ArrayList<>();
    }

    public void invoca(Command solicitare) {

        solicitari.add(solicitare);
    }


    public void executaComanda() {
        if(!solicitari.isEmpty()) {
            solicitari.get(0).executa();
            solicitari.remove(0);

        }
    }

}

