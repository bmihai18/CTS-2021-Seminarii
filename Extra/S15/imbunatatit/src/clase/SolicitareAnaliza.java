package clase;

public class SolicitareAnaliza implements ISolicitareAnaliza {
    private String pacient;
    private String email;
    private TipAnaliza tip;

    public String getPacient() {
        return pacient;
    }

    public SolicitareAnaliza(String pacient, String email, TipAnaliza tip) {
        this.pacient = pacient;
        this.email=email;
        this.tip = tip;
    }




    public String getEmail() {
        return email;
    }


    @Override
    public void analizaProba(TipAnaliza tip, String pacient) {
        if (tip == TipAnaliza.COVID)

        {
            System.out.println("Pacientul" + this.getPacient() + "are mailul"+this.getEmail() + "a solicitat analiza " + TipAnaliza.COVID);
            System.out.println("Analiza dumneavoastra va fi procesata in cel mai scurt timp!");
        }
    else if (tip==TipAnaliza.COLESTEROL){
            System.out.println("Pacientul" + this.getPacient() + "are mailul"+this.getEmail() + "a solicitat analiza " + TipAnaliza.COLESTEROL);

        }
    else {
            System.out.println("Pacientul" + this.getPacient() + "are mailul"+this.getEmail() + "a solicitat analiza " + TipAnaliza.DEFICIT_VITAMINE);

        }
    }
}
