package clase;

public class analizaColesterol extends Command {
   private String nume;

    public analizaColesterol(SolicitareAnaliza solicitare, Integer id, String nume) {
        super(solicitare, id);
        this.nume = nume;
    }

    @Override
    public void executa() {
        super.getSolicitare().analizaProba(TipAnaliza.COLESTEROL, nume);
    }
}
