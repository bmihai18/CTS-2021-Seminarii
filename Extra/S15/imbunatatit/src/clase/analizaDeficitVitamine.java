package clase;

public class analizaDeficitVitamine extends Command {
    private String nume;

    public analizaDeficitVitamine(SolicitareAnaliza solicitare, Integer id, String nume) {
        super(solicitare, id);
        this.nume = nume;
    }

    @Override
    public void executa() {
        super.getSolicitare().analizaProba(TipAnaliza.COVID, nume);

    }
}
