package main;


import clase.*;

public class Program {
    public static void main(String[] args) {
        SolicitareAnaliza solicitare = new SolicitareAnaliza("Mihai","mihai@mihai.eu", TipAnaliza.COVID);
        ManagerSolicitari manager = new ManagerSolicitari();
        manager.invoca(new analizaCOVID(solicitare,90, "Mihai"));
        manager.executaComanda();
        SolicitareAnaliza solicitareAnaliza = new SolicitareAnaliza("Marcelo","marcelo@aws.es", TipAnaliza.COLESTEROL);
        manager.invoca(new analizaColesterol(solicitareAnaliza, 40, "Marcelo"));
        manager.executaComanda();
        manager.executaComanda();
    }
    }

