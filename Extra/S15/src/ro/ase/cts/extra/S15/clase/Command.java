package ro.ase.cts.extra.S15.clase;

public abstract class Command {
    SolicitareAnaliza solicitare;
    Integer id;

    public Command() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Command(SolicitareAnaliza solicitare, Integer id) {
        this.solicitare = solicitare;
        this.id=id;
    }

    public SolicitareAnaliza getSolicitare() {
        return solicitare;
    }


    public abstract void executa();
}
