package ro.ase.cts.extra.S15.clase;

public class SolicitareAnaliza implements ISolicitareAnaliza {
    private String nume;
    private String email;
    private boolean nCov;

    public SolicitareAnaliza(String nume, String email, boolean nCov) {
        this.nume = nume;
        this.email = email;
        this.nCov = nCov;
    }

    public boolean isnCov() {
        return nCov;
    }


    public String getNume() {
        return nume;
    }

    public String getEmail() {
        return email;
    }



    @Override
    public void analizaCOVID(long prioritate) {
        if (isnCov()) {
            System.out.println("Solicitarea de analiza nCOV pe numele " + this.getNume() + " avand emailul " + this.getEmail() + " a fost procesata.");
        }
    }

    @Override
    public void analizeSangeN(long prioritate) {
        if (!isnCov()) {

            System.out.println("Solicitarea de analiza normala de sange pe numele " + this.getNume() + " avand emailul " + this.getEmail() + " a fost adaugata in coada si va fi procesata.");
            System.out.println("Solicitarea de analiza nCOV pe numele " + this.getNume() + " avand emailul " + this.getEmail() + " a fost procesata");

        }
    }

    @Override
    public void analizaColesterol(long prioritate) {
        if (!isnCov()) {

            System.out.println("Solicitarea de analiza colesterol pe numele " + this.getNume() + " avand emailul " + this.getEmail() + " a fost adaugata in coada si va fi procesata ");
            System.out.println("Solicitarea de analiza colesterol pe numele " + this.getNume() + " avand emailul " + this.getEmail() + " a fost procesata ");


        }
    }
}
