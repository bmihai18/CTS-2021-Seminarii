package ro.ase.cts.extra.S15.clase;

public class analizaColesterol extends Command {
    public analizaColesterol() {
    }

    public analizaColesterol(SolicitareAnaliza solicitare, Integer id) {
        super(solicitare, id);
    }

    @Override
    public void executa() {
        super.getSolicitare().analizaColesterol(super.getId());
    }
}
