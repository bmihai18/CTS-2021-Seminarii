package ro.ase.cts.extra.S15.main;

import ro.ase.cts.extra.S15.clase.ManagerSolicitari;
import ro.ase.cts.extra.S15.clase.SolicitareAnaliza;
import ro.ase.cts.extra.S15.clase.analizaCOVID;
import ro.ase.cts.extra.S15.clase.analizaColesterol;

public class Program {
    public static void main(String[] args) {
        SolicitareAnaliza solicitare = new SolicitareAnaliza("Beuca", "mihai@mihai.com", true);
        ManagerSolicitari manager = new ManagerSolicitari();
        manager.invoca(new analizaCOVID(solicitare,90));
        manager.executaComanda();
        SolicitareAnaliza solicitareAnaliza = new SolicitareAnaliza("Marcelo","marcelo@aws.es", false);
        manager.invoca(new analizaColesterol(solicitareAnaliza, 40));
        manager.executaComanda();
        manager.executaComanda();
    }
    }

