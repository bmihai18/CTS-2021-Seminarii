package clase;
import static java.lang.Boolean.TRUE;
import static java.lang.Boolean.FALSE;

//frunza
    public class Mechanics extends NodAbstract {
    private boolean eng;
    private boolean leader;
    private boolean mecAuto;

    public Mechanics(String nume, boolean eng, boolean leader, boolean mecAuto) {
        super(nume);
        this.eng = eng;
        this.leader = leader;
        this.mecAuto = mecAuto;
    }

    @Override
    public void participaLaProiect(String denumire) {
        System.out.println(this.nume + " participa la proiectul " + denumire);
    }

    @Override
    public void intraInPauza() {
        System.out.println(this.nume + " a intrat in pauza");
    }

    @Override
    public void lucreaza(int ore) {
        System.out.println(this.nume + " lucreaza de " + ore + " ore");
    }
}

////////////////v1
//    @Override
//    public void adauga(NodAbstract nod) {
//        throw new UnsupportedOperationException();
//    }
//
//    @Override
//    public void sterge(int index) {
//        throw new UnsupportedOperationException();
//    }
//
//    @Override
//    public NodAbstract get(String nume) {
//        throw new UnsupportedOperationException();
//    }



