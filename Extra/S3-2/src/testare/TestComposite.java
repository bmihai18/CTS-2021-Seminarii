package testare;


import clase.CompositeGrup;
import clase.Mechanics;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class TestComposite {
    public static void main(String[] args) {
        Mechanics gigel = new Mechanics("gigel",TRUE, TRUE, FALSE);
        Mechanics dorel = new Mechanics("dorel",FALSE, FALSE, TRUE);

        CompositeGrup echipaProiect1 = new CompositeGrup("Echipa proiect 1");
        echipaProiect1.adauga(gigel);
        echipaProiect1.adauga(dorel);

        CompositeGrup echipaProiect2 = new CompositeGrup("Echipa proiect 2");
        echipaProiect2.adauga(echipaProiect1);
        echipaProiect2.adauga(new Mechanics("Mihai", TRUE, FALSE, FALSE));


       echipaProiect1.participaLaProiect("Cupa Karting 2021");
       echipaProiect2.participaLaProiect("Best Auto Service");
       echipaProiect1.lucreaza(9);
       echipaProiect2.lucreaza(11);
       echipaProiect1.intraInPauza();
       echipaProiect2.intraInPauza();

    }
}
