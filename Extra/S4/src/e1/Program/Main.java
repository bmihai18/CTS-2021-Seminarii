package e1.Program;


import e1.clase.Customers;
import e1.clase.Factory;
import e1.clase.TipClient;

public class Main {
    public static void main(String args[]) {
        try {
//    	  	Factory factory=new Factory();
          Customers customer = (Customers) Factory.createClient(TipClient.Customer, "Mihai");
          Customers VIPClient = (Customers) Factory.createClient(TipClient.VIPClient, "Andrei");
            System.out.println(customer.toString());
            System.out.println(VIPClient.toString());
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}
