package e1.clase;

public class Factory {
     public static Client createClient(TipClient tipClient, String nume ) throws Exception {
    	 switch(tipClient) {
			 case Customer:
    		 return new Customers(nume);
    	 case LoyalClient:
    		 return new LoyalClients(nume);
			 case VIPClient:
    		 return new VIPClients(nume);
    	 default:
    		 throw new Exception("Nu exista");
    	 }
     }
}
