package teste;

import clase.Grupa;
import clase.Student;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

//Right-BICEP
public class TestGrupa {
    private Grupa grupa;

    @Before
    public void Setup()
    {
        grupa = new Grupa(1077);
        for(int i=0;i<8;i++)
        {
            Student student1 = new Student("Marcel");
            student1.adaugaNota(5);
            student1.adaugaNota(4);
            student1.adaugaNota(10);
            grupa.adaugaStudent(student1);
        }

    }

    @Test
    public void testeGetPromRight(){
        Student student2 = new Student("Gogu");
        student2.adaugaNota(10);
        student2.adaugaNota(10);
        grupa.adaugaStudent(student2);
        Student student3 = new Student("Gigel");
        student3.adaugaNota(9);
        student3.adaugaNota(9);
        grupa.adaugaStudent(student3);
        assertEquals(0.20,grupa.getPromovabilitate(),1);
    }

    @Test
    public void testGetPromBoundaryLOW() {
        Grupa grupa = new Grupa(1077);
        for (int i = 0; i < 18; i++) {
            Student student1 = new Student("Marcel");
            student1.adaugaNota(5);
            student1.adaugaNota(10);
            student1.adaugaNota(10);
            grupa.adaugaStudent(student1);
            grupa.getPromovabilitate();
        }
        assertEquals(1, grupa.getPromovabilitate(),0.5);

    }
    @Test(expected = IllegalArgumentException.class)
    public void testPromError(){
            Grupa grupa1=new Grupa(1066);
            grupa1.getPromovabilitate();

    }

    @Test(timeout=500)
    public void testPromPerf()
    {
        grupa.getPromovabilitate();

    }
            @Test
    public void testConstructorRight() {
        Grupa grupa = new Grupa(1077);
        assertEquals(1077, grupa.getNrGrupa());
    }

@Test
    public void testConstructorBoundaryInf() {
        int nrGrupa=1000;
        Grupa grupa = new Grupa(nrGrupa);
        assertEquals(nrGrupa, grupa.getNrGrupa());
    }
@Test
public void testConstructorBoundarySup(){
        int nrGrupa=1100;
        Grupa grupa = new Grupa(nrGrupa);
        assertEquals(nrGrupa, grupa.getNrGrupa());
    }

    @Test(expected=IllegalArgumentException.class)
    public void testEroriMaiMic(){
        int nrGrupa=100;
        Grupa grupa = new Grupa(nrGrupa);

    }

    @Test(expected=IllegalArgumentException.class)
    public void testEroriMaiMare() {
        int nrGrupa=11001;
        Grupa grupa = new Grupa(nrGrupa);
    }
    @Test(timeout=500)
    public void testPerformance()
    {
        Grupa grupa = new Grupa(1077);
    }
    @Test
    public void testExistence()
    {
        Grupa grupa = new Grupa(1077);
        assertNotNull(grupa.getStudenti());
    }

}
