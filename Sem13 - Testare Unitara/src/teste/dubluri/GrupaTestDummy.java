package teste.dubluri;

import clase.Grupa;
import clase.IStudent;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GrupaTestDummy {
    @Test

    public void testAddStudDummy() {
        IStudent student1 = new StudentDummy();
        IStudent student2 = new StudentDummy();
        Grupa grupa = new Grupa(1136);
        Grupa grupa2 = new Grupa(1137);
        grupa.adaugaStudent(student1);
        assertEquals(1, grupa.getStudenti().size());

    }

}
