package teste.dubluri;

import clase.Grupa;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import teste.suteCatego.TesteRandomCategory;

import static org.junit.Assert.assertEquals;

public class GrupaTestFake  {

    @Test
    @Category({TesteRandomCategory.class})
    public void testGetPromFake()
    {
        Grupa grupa = new Grupa(1001);
        StudentFake studentFake = new StudentFake();
//        StudentFake stud = mock
        studentFake.setValAreRestante(true);
        grupa.adaugaStudent(studentFake);

        StudentFake student1Fake = new StudentFake();
        student1Fake.setValAreRestante(false);
        grupa.adaugaStudent(student1Fake);

        StudentFake student2Fake = new StudentFake();
        student2Fake.setValAreRestante(false);
        grupa.adaugaStudent(student2Fake);
        assertEquals(0.66, grupa.getPromovabilitate(), 0.5);

//        StudentFake studentFake1 = mock()


    }
}
