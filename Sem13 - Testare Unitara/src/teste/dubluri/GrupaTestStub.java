package teste.dubluri;

import clase.Grupa;
import clase.IStudent;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import teste.suteCatego.ClassicTestCategory;
import teste.suteCatego.TesteRandomCategory;

import static org.junit.Assert.assertEquals;

public class GrupaTestStub {
    @Test
    @Category({TesteRandomCategory.class})
    public void testGetPromStub() {
        Grupa grupa = new Grupa(1001);
        for(int i =0;i<25;i++) {
            IStudent student = new StudentStub();
            grupa.adaugaStudent(student);

        }
        assertEquals(0, grupa.getPromovabilitate(),0.01);
    }
    @Test
    @Category({ClassicTestCategory.class})
    public void testGetPromStub2() {
        Grupa grupa = new Grupa(1001);
        for(int i =0;i<25;i++) {
            IStudent student = new StudentStub();
            grupa.adaugaStudent(student);

        }
        assertEquals(0, grupa.getPromovabilitate(),0.01);
    }
}
