package teste.dubluri;

import clase.IStudent;

import java.util.List;

import static java.lang.Boolean.TRUE;

public class StudentFake implements IStudent {
    private float returnMedie;
    private boolean valAreRestante;

    public void setReturnMedie(float returnMedie) {
        this.returnMedie = returnMedie;
    }

    public StudentFake() {
        this.valAreRestante = TRUE;
        this.returnMedie = (float) 8.8;
    }

    public void setValAreRestante(boolean valAreRestante) {
        this.valAreRestante = valAreRestante;
    }

    @Override
    public String getNume() {
        return null;
    }

    @Override
    public void setNume(String nume) {

    }

    @Override
    public List<Integer> getNote() {
        return null;
    }

    @Override
    public void adaugaNota(int nota) {

    }

    @Override
    public float calculeazaMedie() {
        return returnMedie;
    }

    @Override
    public int getNota(int index) {
        return 0;
    }

    @Override
    public boolean areRestante() {
        return valAreRestante;
    }
}
