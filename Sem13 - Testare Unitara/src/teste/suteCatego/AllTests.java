package teste.suteCatego;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import teste.dubluri.GrupaTestFake;
import teste.dubluri.GrupaTestStub;


@RunWith(Suite.class)
@SuiteClasses({ GrupaTestFake.class, GrupaTestStub.class })
public class AllTests {

}
