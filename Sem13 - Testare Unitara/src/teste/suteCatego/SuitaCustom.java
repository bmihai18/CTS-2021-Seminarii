package teste.suteCatego;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import teste.dubluri.GrupaTestFake;
import teste.dubluri.GrupaTestStub;

@RunWith(Categories.class)
@Suite.SuiteClasses({GrupaTestFake.class, GrupaTestStub.class})
@Categories.IncludeCategory({TesteRandomCategory.class})
public class SuitaCustom {
}
