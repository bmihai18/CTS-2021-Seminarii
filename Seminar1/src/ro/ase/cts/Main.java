package ro.ase.cts;
import ro.ase.cts.clase.Animal;
import ro.ase.cts.clase.Lion;
import ro.ase.cts.clase.WildCat;
import ro.ase.cts.clase.Zoo;
public class Main {
public static void main(String[] args) {
    Zoo zoo = new Zoo();
    Lion lion1 = new Lion("leu");
    Lion lion2 = new Lion("altLeu");
    Animal lion3 = new Lion("RegeleLeu");
    zoo.addAnimal(lion1);
    zoo.addAnimal(lion2);
    zoo.feedAllAnimals();

    WildCat aSmallCat = new WildCat("kitty");
    WildCat aFurballCat = new WildCat("softkitty");
    //MET PROC N-AU OPERATII LA CONSOLA
    zoo.addAnimal(aSmallCat);
    zoo.addAnimal(aFurballCat);
    zoo.feedAllAnimals();
    //Interfata are doar fct virtuale, in Java are doar metode abstracte
}
}
