package ro.ase.cts.E15command.clase;

public class ComConstituire extends Command {

    public ComConstituire(ContBancar cont, float suma) {
        super(cont, suma);
    }

    @Override
    public void executa() {
        super.getCont().constituire(super.getSuma());
    }
}
