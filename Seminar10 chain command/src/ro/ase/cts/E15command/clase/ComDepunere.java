package ro.ase.cts.E15command.clase;

public class ComDepunere extends Command {

    public ComDepunere(ContBancar contBancar, float suma) {
        super(contBancar, suma);
    }

    @Override
    public void executa() {
        super.getCont().depunere(super.getSuma());

    }

}

