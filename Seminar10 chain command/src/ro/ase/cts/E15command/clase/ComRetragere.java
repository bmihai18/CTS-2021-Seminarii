package ro.ase.cts.E15command.clase;


public class ComRetragere extends Command {

    public ComRetragere(ContBancar contBancar, float suma) {
        super(contBancar, suma);
    }

    @Override
    public void executa() {
        super.getCont().retragere(super.getSuma());
    }





}