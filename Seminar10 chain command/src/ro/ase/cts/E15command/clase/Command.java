package ro.ase.cts.E15command.clase;

public abstract class Command {
    ContBancar cont;
    float suma;

    public Command() {
    }

    public Command(ContBancar cont, float suma) {
        this.cont = cont;
        this.suma = suma;
    }

    public ContBancar getCont() {
        return cont;
    }

    public void setCont(ContBancar cont) {
        this.cont = cont;
    }

    public float getSuma() {
        return suma;
    }

    public void setSuma(float suma) {
        this.suma = suma;
    }

    public abstract void executa();
}
