package ro.ase.cts.E15command.main;

import ro.ase.cts.E15command.clase.ComConstituire;
import ro.ase.cts.E15command.clase.ComRetragere;
import ro.ase.cts.E15command.clase.ContBancar;
import ro.ase.cts.E15command.clase.ManagerComenzi;

public class ProgramOperator {
    public static void main(String[] args) {
        ContBancar cont = new ContBancar("Beuca");
        ManagerComenzi manager = new ManagerComenzi();
        manager.invoca(new ComConstituire(cont,90));
        manager.executaComanda();
        manager.invoca(new ComRetragere(cont, 40));
        manager.executaComanda();
        manager.executaComanda();
    }
}
