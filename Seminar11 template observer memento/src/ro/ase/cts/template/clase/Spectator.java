package ro.ase.cts.template.clase;

public class Spectator extends SpectatorAbstract {
    private String nume;

    public Spectator(String nume) {
        this.nume = nume;
    }

    @Override
    public void asezareCoada() {
        System.out.println("Spectatorul " + nume + "se va aseza la coada");
    }

    @Override
    public void prezintaBilet() {
        System.out.println("Spectatorul " + nume + "va cumpara biletul");

    }

    @Override
    public void realizareControlCorporal() {
        System.out.println("Spectatorul " + nume + "va fi controlat corporal");

    }

    @Override
    public void intraPeStadion() {
        System.out.println("Spectatorul " + nume + "va intra pe stadion");

    }

    @Override
    public void ocupaLoc() {
        System.out.println("Spectatorul " + nume + "se va ocupa locul");

    }
}
