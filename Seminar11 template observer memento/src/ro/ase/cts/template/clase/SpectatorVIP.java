package ro.ase.cts.template.clase;

public class SpectatorVIP extends SpectatorAbstract
{
    public SpectatorVIP(String numeVIP) {
        this.numeVIP = numeVIP;
    }

    private String numeVIP;

    @Override
    public void asezareCoada() {
        System.out.println("Spectatorul " + numeVIP + "se va aseza la usa");

    }

    @Override
    public void prezintaBilet() {
        System.out.println("Spectatorul " + numeVIP + "va prezenta biletul VIP");

    }

    @Override
    public void realizareControlCorporal() {
        System.out.println("Spectatorul " + numeVIP + "este controlat corporal");


    }

    @Override
    public void intraPeStadion() {
        System.out.println("Spectatorul " + numeVIP + "intra pe stadion");

    }

    @Override
    public void ocupaLoc() {
        System.out.println("Spectatorul " + numeVIP + "se va aseza in loja");


    }
}
