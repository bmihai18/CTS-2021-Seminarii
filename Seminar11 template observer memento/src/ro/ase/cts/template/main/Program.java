package ro.ase.cts.template.main;

import ro.ase.cts.template.clase.Spectator;
import ro.ase.cts.template.clase.SpectatorVIP;

public class Program {
    public static void main(String[] args) {
        Spectator s1 = new Spectator("Marcel");
        s1.intrareSpectatorStadion();
        SpectatorVIP s2 = new SpectatorVIP("Mihai");
        s2.intrareSpectatorStadion();
    }
}
