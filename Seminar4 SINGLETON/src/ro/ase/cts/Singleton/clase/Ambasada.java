package ro.ase.cts.Singleton.clase;

public class Ambasada {
    public double suprafata;
    public String tara;
    public int numarAngajati;

    private static Ambasada ambasada = null;

    private Ambasada(double suprafata, String tara, int numarAngajati){
        this.suprafata = suprafata;
        this.tara = tara;
        this.numarAngajati = numarAngajati;
    }

    public static synchronized Ambasada getInstance(double suprafata, String tara, int numarAngajati) {
        if (ambasada == null) {
            ambasada = new Ambasada(suprafata , tara, numarAngajati);
        }
        return ambasada;
    }

    @Override
    public String toString() {
        return "Ambasada{" +
                "suprafata='" + suprafata + '\'' +
                ", tara='" + tara + '\'' +
                ", numarAngajati=" + numarAngajati +
                '}';
    }

    public void setSuprafata (double suprafata){
            this.suprafata = suprafata;
        }

        public void setTara (String tara){
            this.tara = tara;
        }

        public void setNumarAngajati ( int numarAngajati){
            this.numarAngajati = numarAngajati;
        }


    }

