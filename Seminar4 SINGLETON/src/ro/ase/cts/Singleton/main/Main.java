package ro.ase.cts.Singleton.main;

import ro.ase.cts.Singleton.clase.Ambasada;
import ro.ase.cts.Singleton.clase.DepartamentFinanciar;
import ro.ase.cts.Singleton.clase.DepartamentFinanciarEager;

public class Main {

    public static void main(String[] args) {

        DepartamentFinanciarEager departamentFinanciarEager1= DepartamentFinanciarEager.getInstance();
        DepartamentFinanciarEager departamentFinanciarEager2= DepartamentFinanciarEager.getInstance();
        System.out.println(departamentFinanciarEager1.toString());
        System.out.println(departamentFinanciarEager2.toString());

        departamentFinanciarEager1.setDirector("Gigel");
        departamentFinanciarEager2.setNrAngajati(20);
        System.out.println(departamentFinanciarEager1.toString());
        System.out.println(departamentFinanciarEager2.toString());

        DepartamentFinanciar departamentFinanciar1=DepartamentFinanciar.getInstance(20, "Popescu", 2500);
        DepartamentFinanciar departamentFinanciar2=DepartamentFinanciar.getInstance(30, "Antonescu", 2700);

        System.out.println(departamentFinanciar1.toString());
        System.out.println(departamentFinanciar2.toString());
        Ambasada ambasada = Ambasada.getInstance(111.5, "S.U.A", 22);
        Ambasada ambasada2 = Ambasada.getInstance(121.3, "Anglia", 300);
        System.out.println(ambasada.toString());
        System.out.println(ambasada2.toString());



    }

}