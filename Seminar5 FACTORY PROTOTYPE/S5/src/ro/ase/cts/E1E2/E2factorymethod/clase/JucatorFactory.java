package ro.ase.cts.E1E2.E2factorymethod.clase;

public interface JucatorFactory {
    Jucator creareJucator(String nume);
}
