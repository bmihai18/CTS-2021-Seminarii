package ro.ase.cts.E1E2.E2factorymethod.clase;

    public class MijlocasFactory implements JucatorFactory {
        @Override
        public Jucator creareJucator(String nume) {
            return new Mijlocas(nume);

        }
}
