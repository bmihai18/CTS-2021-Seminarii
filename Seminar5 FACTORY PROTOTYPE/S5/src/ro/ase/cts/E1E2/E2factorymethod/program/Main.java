package ro.ase.cts.E1E2.E2factorymethod.program;

import ro.ase.cts.E1E2.E2factorymethod.clase.FundasFactory;
import ro.ase.cts.E1E2.E2factorymethod.clase.Jucator;
import ro.ase.cts.E1E2.E2factorymethod.clase.JucatorFactory;
import ro.ase.cts.E1E2.E2factorymethod.clase.PortarFactory;

public class Main {
    public static void printeazaJucator(JucatorFactory factory, String nume) {
        Jucator jucator1 = factory.creareJucator(nume);
        System.out.println(jucator1);

    }
    public static void main(String[] args) {
        printeazaJucator(new PortarFactory(), "Neuer");
        printeazaJucator(new FundasFactory(),"Gabriel");
    }
}
