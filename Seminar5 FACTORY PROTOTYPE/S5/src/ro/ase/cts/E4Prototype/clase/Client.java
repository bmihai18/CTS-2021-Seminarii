package ro.ase.cts.E4.clase;

public class Client {
    private String nume;
    private String nationalitate;
    private int nrClient;
    private int varsta;

    public Client(String nume, String nationalitate, int nrClient, int varsta) {
        this.nume = nume;
        this.nationalitate = nationalitate;
        this.nrClient = nrClient;
        this.varsta = varsta;
    }

    public Client() {
    }

    public ClientPrototype copiaza() {
        Client client = new Client();
        client.nume = this.nume;
        client.nationalitate = this.nationalitate;
        client.nrClient = this.nrClient;
        client.varsta = this.varsta;
return copiaza();

    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getNationalitate() {
        return nationalitate;
    }

    public void setNationalitate(String nationalitate) {
        this.nationalitate = nationalitate;
    }

    public int getNrClient() {
        return nrClient;
    }

    public void setNrClient(int nrClient) {
        this.nrClient = nrClient;
    }

    public int getVarsta() {
        return varsta;
    }

    @Override
    public String toString() {
        return "Client{" +
                "nume='" + nume + '\'' +
                ", nationalitate='" + nationalitate + '\'' +
                ", nrClient=" + nrClient +
                ", varsta=" + varsta +
                '}';
    }

    public void setVarsta(int varsta) {
        this.varsta = varsta;
    }


}
