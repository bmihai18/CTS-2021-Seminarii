package ro.ase.cts.E4.program;

import ro.ase.cts.E4.clase.Client;

public class Main {
    public static void main(String[] args) {
        Client client1 = new Client("Gigel", "ro", 2001, 11);
        Client client2 = (Client) client1.copiaza();
        System.out.println(client1.toString());
        System.out.println(client2.toString());

    }
}
//Proto