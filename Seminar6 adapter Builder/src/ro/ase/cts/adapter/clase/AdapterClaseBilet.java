package ro.ase.cts.adapter.clase;

//Is-a

public class AdapterClaseBilet extends Bilet implements BiletOnline {

	public AdapterClaseBilet(float pret) {
		super(pret);

	}

	@Override
	public void vindeBilet() {
		super.vinde();
	}

	@Override
	public void rezervaBilet() {
		super.rezerva();
	}

}
