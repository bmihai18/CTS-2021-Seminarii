package ro.ase.cts.adapter.program;

import ro.ase.cts.adapter.clase.AdapterClaseBilet;
import ro.ase.cts.adapter.clase.AdapterObiectBilet;
import ro.ase.cts.adapter.clase.Bilet;
import ro.ase.cts.adapter.clase.BiletOnline;

public class Program {

	public static void rezervaSiVindeBiletOnline(BiletOnline biletOnline) {
		biletOnline.rezervaBilet();
		biletOnline.vindeBilet();
	}

	public static void rezervaSiVindeBiletLaCasa(Bilet bilet) {
		bilet.rezerva();
		bilet.vinde();
	}

	public static void main(String[] args) {
		Bilet bilet = new Bilet(30);
		rezervaSiVindeBiletLaCasa(bilet);

		BiletOnline biletOnline = new AdapterClaseBilet(40);
		rezervaSiVindeBiletOnline(biletOnline);

		AdapterObiectBilet adapterObiectBilet = new AdapterObiectBilet(bilet);
		rezervaSiVindeBiletOnline(adapterObiectBilet);

	}

}
