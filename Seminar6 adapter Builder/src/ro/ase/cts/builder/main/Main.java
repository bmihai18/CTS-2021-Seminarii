package ro.ase.cts.builder.main;

import ro.ase.cts.builder.clase.Rezervare;
import ro.ase.cts.builder.clase.RezervareBuilder;

public class Main {

    public static void main(String[] args) {
        RezervareBuilder rezervareBuilder = new RezervareBuilder();
        Rezervare rezervare1 = rezervareBuilder.setAreBauturaInclusa(true).build();
        Rezervare rezervare2 = rezervareBuilder.setAreMuzicaAmbientala(true).setGenMuzica("Pop").build();
        System.out.println(rezervare1);
        System.out.println(rezervare2);
//second builder for 2nd and newer rezervare
        Rezervare rezervare3 = new RezervareBuilder().setAreMuzicaAmbientala(true).setGenMuzica("Rock").build();
    }

}
