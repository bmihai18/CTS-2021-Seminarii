package ro.ase.cts.Facade.clase;

public class Bilet {
    String nume;
    String numeLoc;

    public Bilet(String nume, String numeLoc) {
        this.nume = nume;
        this.numeLoc = numeLoc;
    }

    public String getNume() {
        return nume;
    }

}
