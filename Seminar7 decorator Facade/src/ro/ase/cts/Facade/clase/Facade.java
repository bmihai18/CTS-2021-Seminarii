package ro.ase.cts.Facade.clase;


    public class Facade {
        public static boolean iSePermiteAccesul(Persoana persoana,Bilet bilet)
        {
            if(bilet.getNume().equals(persoana.getNume())){
                if(Politie.esteUrmarita(persoana)){
                    return BDHuligani.esteHuligan(persoana);
                }
            }
            return false;
        }
    }

