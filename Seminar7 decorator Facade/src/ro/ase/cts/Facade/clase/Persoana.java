package ro.ase.cts.Facade.clase;

public class Persoana {
    public Persoana(String nume, String CNP) {
        this.nume = nume;
        this.CNP = CNP;
    }

    public String getNume() {
        return nume;
    }

    public String getCNP() {
        return CNP;
    }

    private String nume;
    private String CNP;
}


