package ro.ase.cts.Facade.main;

import ro.ase.cts.Facade.clase.*;

public class Program {
    public static void main(String[] args) {
        Persoana p1 = new Persoana("Mihai", "190000000000");
        Bilet b1 = new Bilet("Andrei", "1A");

        if(p1.getNume().equals(b1.getNume())) {
            if(Politie.esteUrmarita(p1)) {
                if(BDHuligani.esteHuligan(p1))
                {
                    System.out.println("Poftiti");
                }

            }
        }
Persoana p2 = new Persoana("Maria", "2900000000000001");
        Bilet b2 = new Bilet("Maria", "E4");
        if (Facade.iSePermiteAccesul(p2, b2))
        {
                System.out.println("Poftiti");

        }
        else {
            System.out.println("Nu aveti voie");
        }
    }
}
