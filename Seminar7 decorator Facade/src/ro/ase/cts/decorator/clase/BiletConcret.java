package ro.ase.cts.decorator.clase;

public class BiletConcret implements BiletAbstract {
    private String numeGazda;
    private String numeOaspeti;
    private String numeClient;


    public BiletConcret(String numeGazda, String numeOaspeti, String numeClient) {
        this.numeGazda = numeGazda;
        this.numeOaspeti = numeOaspeti;
        this.numeClient = numeClient;
    }




public void rezervaBilet() {
    System.out.println("Clientul cu numele" + numeClient + "are bilet la meciul" + numeGazda + "vs" + numeOaspeti );
}

    public String getNumeGazda() {
        return numeGazda;
    }

    public void setNumeGazda(String numeGazda) {
        this.numeGazda = numeGazda;
    }

    public String getNumeOaspeti() {
        return numeOaspeti;
    }

    public void setNumeOaspeti(String numeOaspeti) {
        this.numeOaspeti = numeOaspeti;
    }

    public String getNumeClient() {
        return numeClient;
    }

    public void setNumeClient(String numeClient) {
        this.numeClient = numeClient;
    }
}