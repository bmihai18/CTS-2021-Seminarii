package ro.ase.cts.decorator.program;

import ro.ase.cts.decorator.clase.BiletConcret;
import ro.ase.cts.decorator.clase.DecoratorAbstract;
import ro.ase.cts.decorator.clase.DecoratorCuMesajSustinere;
import ro.ase.cts.decorator.clase.DecoratorMesajLMA;

public class Program {

    public static void main(String[] args) {
        BiletConcret bilet = new BiletConcret("Steaua", "Rapid", "Andrei");
        bilet.rezervaBilet();

        System.out.println("---------------------------------------");

        DecoratorCuMesajSustinere decorator1 = new DecoratorCuMesajSustinere(bilet);
        decorator1.rezervaBilet();

        System.out.println("---------------------------------------");

        DecoratorMesajLMA decorator2 = new DecoratorMesajLMA(bilet);
        decorator2.rezervaBilet();

        System.out.println("---------------------------------------");

        DecoratorAbstract decorator3 = new DecoratorMesajLMA(decorator1);
        decorator3.rezervaBilet();

    }

}
