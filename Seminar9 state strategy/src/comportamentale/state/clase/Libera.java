package comportamentale.state.clase;

public class Libera implements Stare {
    @Override
    public void schimbaStare(Masa masa) {
        if (!(masa.getStare() instanceof Libera)) {
            System.out.println("a fost el");
            masa.setStare(this);
        } else {
            System.out.println("e deja");
        }
    }
}
