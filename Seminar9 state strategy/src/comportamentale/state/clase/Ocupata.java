package comportamentale.state.clase;

public class Ocupata implements Stare {
    @Override
    public void schimbaStare(Masa masa) {
        if (!(masa.getStare() instanceof Ocupata)) {
            System.out.println("ati oc");
            masa.setStare(this);
        } else {
            System.out.println("nu poti");

        }
    }
}
