package comportamentale.strategy.clase;

public class Card implements ModalitatePlata {
    private float sold;


    public Card(float sold) {
        this.sold = sold;
    }

    @Override
    public void achita(float pret) {
        if (sold <= pret) {
            System.out.println("am platit cu cardul pretul" + pret);
            sold -= pret;
        }
    }
}