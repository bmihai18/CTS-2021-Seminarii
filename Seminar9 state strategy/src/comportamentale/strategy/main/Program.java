package comportamentale.strategy.main;

import comportamentale.strategy.clase.Card;
import comportamentale.strategy.clase.Cash;
import comportamentale.strategy.clase.Client;

public class Program {
    public static void main(String[] args) {
        Card card = new Card(73);
        Client client = new Client("Mete-Emre", card);

        client.achitaNota(50);

        Cash cash = new Cash();

        client.setModalitatePlata(cash);
        client.achitaNota(40);

        client.setModalitatePlata(card);
        client.achitaNota(30);
    }
}
